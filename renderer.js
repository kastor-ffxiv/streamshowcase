const electron = require('electron')
const ipc = electron.ipcRenderer
let config = {};

const saveButton = document.querySelector('#save')
saveButton.addEventListener('click', () => {
    closePreview();
    let imageDir = document.getElementById('imageDir').value;
    let timeout = document.getElementById('timeout').value;
    let backgroundColor = document.getElementById('backgroundColor').value;
    let port = document.getElementById('port').value;

    const reply = ipc.sendSync('sync-message', {
        type: 'update-config',
        settings: {
            imageDir: imageDir,
            timeout: timeout,
            backgroundColor: backgroundColor,
            port: port
        }
    })

    console.log(reply)
    setCurrentValues();
})

const previewButton = document.querySelector('#preview')
previewButton.addEventListener('click', () => {
    const reply = ipc.sendSync('sync-message', {
        type: 'load-preview',
    })
    console.log(reply)
})

document.addEventListener('DOMContentLoaded', (event) => {
    const reply = ipc.sendSync('sync-message', {
        type: 'config-values'
    })

    if(reply.hasOwnProperty('imageDir')) {
        setCurrentValues(reply)
    }
});

function setCurrentValues(current) {
    config = current;

    const imageDirInput = document.getElementById('imageDir');
    const timeoutInput = document.getElementById('timeout');
    const backgroundColor = document.getElementById('backgroundColor');
    const port = document.getElementById('port');

    const urlPort = document.getElementById('url_port');
    const colorHelp = document.getElementById('color_help');

    imageDirInput.value = config.imageDir;
    timeoutInput.value = config.timeout;
    backgroundColor.value = config.backgroundColor;
    port.value = config.port;

    urlPort.innerText = config.port;
    colorHelp.innerText = config.backgroundColor;
}

function closePreview() {
    const reply = ipc.sendSync('sync-message', {
        type: 'close-preview',
    })
    console.log(reply)
}