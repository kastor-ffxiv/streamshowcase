# StreamShowcase
A browsersource host for streaming image galleries.

## Running The Application

### Windows
Download the latest installer exe from the [Releases](https://gitlab.com/kastor-ffxiv/streamshowcase/-/releases) section and run.

This will install the application, creating a Start Menu & Desktop shortcut, then open the application.

### Linux and MacOS
You will need to check out the source and compile the application via the "Packaging" steps in 
the Development section below.

## Development
Install dependencies via npm `npm install` or your package manager of choice.

### Packaging
The application uses [electron-forge](https://www.electronforge.io/configuration#packager-config) 
and the [squirrel](https://js.electronforge.io/maker/squirrel/interfaces/makersquirrelconfig) maker plugin.

Adjust the configuration to your platform in `package.json` and then run
`npm run make`, an executable will be generated in the `out/` directory.

### Architecture
- The main application is handled in `index.js`
- The settings window is handled in `renderer.js`, `renderer.html` and `renderer.css`
- The browser host server is handled in `server/server.js`
- The browser host template is `server/showcase.html`

[Synchronous IPC](https://www.electronjs.org/docs/latest/api/ipc-main) is used to communicate between different parts of the application.
The majority of this handling is in `index.js:144`

## Screenshots
![Preview](https://static.kastor-ffxiv.com/stream-showcase/preview.png)
![Preview 2](https://static.kastor-ffxiv.com/stream-showcase/preview2.jpg)

## Contributions
You are free to submit pull requests, or fork and create your own version of this application.
