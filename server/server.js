let config = require("../config")
const express = require('express')
const fs = require('fs')
const path = require('path')
const app = express()
let imageMap = [];

config = loadConfig();

const port = config.port;

buildImageMap();

app.get('/', (req, res) => {
    let index = fs.readFileSync(path.join(__dirname + '/showcase.html'), 'utf8');

    index = index.replace('8069', config.port); //MODIFY THE FILE AS A STRING HERE
    return res.send(index);
})

// Send image map to frontend
app.get('/images', async (req, res) => {
    res.send(imageMap)
})

// Send config map to frontend
app.get('/config', (req, res) => {
    res.send(config);
})

// Send static image
app.get('/image/:fileId', (req, res) => {

    if(!imageMap.hasOwnProperty(req.params.fileId)) {
        res.status(404);
        return;
    }

    res.sendFile(config.imageDir + '/' + imageMap[req.params.fileId].filename);
})

function packageImage(file) {
    let artist = ''
    if(!file.startsWith('-')) {
        artist = path.parse(file).name;
        if(artist.indexOf(' ')) {
            let parts = artist.split(' ');
            artist = parts[0];
        }
    }
    return {
        artist: artist,
        filename: file,
    };
}

function buildImageMap() {
    let source = config.imageDir

    try {
        fs.readdirSync(source, {withFileTypes: true})
            .filter(entry => entry.isFile())
            .forEach((dirent) => {
                imageMap.push(packageImage(dirent.name));
            })
    } catch(e) {
        if(e.code === 'ENOENT') {
            console.error('Invalid directory provided: ', config.imageDir)
        } else {
            console.error('Could not stat directory.', e)
        }
    }
}

function getUserConfigLocation() {
    return path.join(path.resolve(__dirname, '..'),'userconfig.json');
}

function loadConfig() {
    try {
        if (fs.existsSync(getUserConfigLocation())) {
            let tempConfig = JSON.parse(fs.readFileSync(getUserConfigLocation(), 'utf8'));
            if(tempConfig.hasOwnProperty('imageDir') && tempConfig.hasOwnProperty('port') && tempConfig.hasOwnProperty('timeout')) {
                return JSON.parse(fs.readFileSync(getUserConfigLocation(), 'utf8'));
            }
        }
    } catch(err) {
        console.warn("No userconfig.json found, using default config.")
    }
}

app.listen(port, () => {
    console.log(`Fileserver listening at http://localhost:${port}`)
})