const { app, BrowserWindow, ipcMain, Tray, Menu } = require('electron')

// Handle Squirrel
if (require('electron-squirrel-startup')) return;

if (handleSquirrelEvent()) {
    // squirrel event handled and app will exit in 1000ms, so don't do anything else
    return;
}

function handleSquirrelEvent() {
    if (process.argv.length === 1) {
        return false;
    }

    const ChildProcess = require('child_process');
    const path = require('path');

    const appFolder = path.resolve(process.execPath, '..');
    const rootAtomFolder = path.resolve(appFolder, '..');
    const updateDotExe = path.resolve(path.join(rootAtomFolder, 'Update.exe'));
    const exeName = path.basename(process.execPath);

    const spawn = function(command, args) {
        let spawnedProcess, error;

        try {
            spawnedProcess = ChildProcess.spawn(command, args, {detached: true});
        } catch (error) {}

        return spawnedProcess;
    };

    const spawnUpdate = function(args) {
        return spawn(updateDotExe, args);
    };

    const squirrelEvent = process.argv[1];
    switch (squirrelEvent) {
        case '--squirrel-install':
        case '--squirrel-updated':
            // Optionally do things such as:
            // - Add your .exe to the PATH
            // - Write to the registry for things like file associations and
            //   explorer context menus

            // Install desktop and start menu shortcuts
            spawnUpdate(['--createShortcut', exeName]);

            setTimeout(app.quit, 1000);
            return true;

        case '--squirrel-uninstall':
            // Undo anything you did in the --squirrel-install and
            // --squirrel-updated handlers

            // Remove desktop and start menu shortcuts
            spawnUpdate(['--removeShortcut', exeName]);

            setTimeout(app.quit, 1000);
            return true;

        case '--squirrel-obsolete':
            // This is called on the outgoing version of your app before
            // we update to the new version - it's the opposite of
            // --squirrel-updated

            app.quit();
            return true;
    }
}

// Handle App
const path = require('path')
let config = require(path.join(app.getAppPath(), 'config.js'))
const childProcess = require('child_process')
const fs = require('fs')

let server;
let mainwindow;
let isQuiting;
let windows = [];

let tray;

loadConfig();

const createWindow = () => {
    // Create tray menu
    tray = new Tray(path.join(__dirname, 'icon.png'));
    tray.setContextMenu(Menu.buildFromTemplate([
        {
            label: 'Settings', click: function () {
                mainwindow.show();
            }
        },
        {
            label: 'Quit', click: function () {
                isQuiting = true;
                app.quit();
            }
        }
    ]));

    // Create main window
    mainwindow = new BrowserWindow({
        width: 800,
        height: 600,
        icon: path.join(__dirname, 'icon.png'),
        webPreferences: {
            preload: path.join(app.getAppPath(), 'preload.js'),
            nodeIntegration: true,
            contextIsolation: false,
            enableRemoteModule: true,
        }
    })

    mainwindow.loadFile('renderer.html')
    mainwindow.removeMenu()

    // Minimize to tray on close button
    mainwindow.on('close', function (event) {
        if (!isQuiting) {
            event.preventDefault();
            mainwindow.hide();
            event.returnValue = false;
        }
    });

    // Start the browser server
    if(config.imageDir !== '') {
        startServer();
    }
}

app.on('before-quit', function () {
    isQuiting = true;
});

app.whenReady().then(() => {
    createWindow()
})

ipcMain.on('sync-message', (event, arg) => {
    if(!arg.hasOwnProperty('type')) {
        event.returnValue = 'unknown-message'
        return;
    }
    switch (arg.type) {
        case 'restart':
            restartServer();
            event.returnValue = 'restart-received'
            return;
        case 'update-config':
            updateConfig('imageDir', arg.settings.imageDir);
            updateConfig('timeout', arg.settings.timeout);
            updateConfig('backgroundColor', arg.settings.backgroundColor);
            updateConfig('port', arg.settings.port);
            event.returnValue = 'config-update-received'
            return;
        case 'config-values':
            event.returnValue = config
            return;
        case 'load-preview':
            windows[1] = loadPreview();
            event.returnValue = 'load-preview-received'
            return;
        case 'close-preview':
            if(typeof windows[1] != 'undefined' && !windows[1].isDestroyed()) {
                windows[1].close();
            }
            event.returnValue = 'close-preview-received'
            return;
        default:
            event.returnValue = 'unknown-message'
            return;
    }
})

function startServer() {
    server = childProcess.fork(app.getAppPath() + '/server/server.js')
    server.on('message', (m) => {
        console.log('Got IPC message from server: ', m)
    })
}

function restartServer() {
    if(server instanceof childProcess.ChildProcess) {
        server.kill('SIGINT');
        startServer();
    }
}

function updateConfig(prop, val) {
    config[prop] = val;
    saveConfig();
}

function saveConfig() {
    fs.writeFile(path.join(app.getAppPath(), 'userconfig.json'), JSON.stringify(config), 'utf8', (e) => {
        if(e) {
            console.error('Failed to save config data.', e)
        }
    });

    restartServer()
}

function loadConfig() {
    try {
        if (fs.existsSync(path.join(app.getAppPath(), 'userconfig.json'))) {
            let tempConfig = JSON.parse(fs.readFileSync(path.join(app.getAppPath(), 'userconfig.json'), 'utf8'));
            if(tempConfig.hasOwnProperty('imageDir') && tempConfig.hasOwnProperty('port') && tempConfig.hasOwnProperty('timeout')) {
                config = tempConfig;
            }
        }
    } catch(err) {
        console.warn("No userconfig.json found, using default config.")
    }
}

function loadPreview() {
    const preview = new BrowserWindow({
        width: 800,
        height: 600,
        x: 500,
        y: 500,
        title: 'Preview'
    })

    preview.loadURL(`http://localhost:${config.port}`)
    preview.removeMenu()

    return preview;
}